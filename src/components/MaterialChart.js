import React from 'react';
import {BarChart} from './BarChart';

export default function(props) {
  if (!props.data) return null;

  let data = {
    label: "Material",
    values: Object.keys(props.data).map((key) => {
      return {
        name: key,
        value: props.data[key].length
      }
    })
  }

  return (
    <div className="material chart-holder">
      <BarChart
        data={data.values}
        title="Material"
        sendFilter={props.sendFilter}
        filter={props.filter}
      />
    </div>
  );
}

