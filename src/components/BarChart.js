import React from 'react';

// Adapted from https://codepen.io/frontendcharts/embed/jpGYoV?height=265&theme-id=0&slug-hash=jpGYoV&default-tab=js%2Cresult&user=frontendcharts&pen-title=React%20bar%20chart&preview=true

function BarGroup(props) {
  let barPadding = 2
  let widthScale = d => d > 0 && 300 * (d / props.max)

  console.log('xxx', props.d)
  let width = widthScale(props.d.value)
  let yMid = props.barHeight * 0.5
  
  let sendFilter = () => {
    props.sendFilter(props.d.name)
  }

  return (
    <g className="bar-group">
      <text className="name-label" x="-6" y={yMid} alignmentBaseline="middle" >{props.d.name}</text>
      <rect onClick={sendFilter} className={"bar-anim " + (props.filter === props.d.name ? "selected" : "")} y={barPadding * 0.5} style={{width}} height={props.barHeight - barPadding} />
      <text className="value-label" x={8} y={yMid} alignmentBaseline="middle" >{props.d.value}</text>
    </g>
  );
}

export class BarChart extends React.Component {
  
  render() {
    let barHeight = 30
        
    let max = 0;

    for (var i = 0; i < this.props.data.length; i++) {
      if (this.props.data[i].value > max) {
        max = this.props.data[i].value;
      }
    }

    let barGroups = this.props.data.map((d, i) => <g key={d.name} transform={`translate(0, ${i * barHeight})`}>
                                                    <BarGroup filter={this.props.filter} sendFilter={this.props.sendFilter} max={max} d={d} barHeight={barHeight} />
                                                  </g>);                         
    
    return <svg width="400" height="300">
      <g className="container">
        <g className="chart" transform="translate(100,60)">
          {barGroups}
        </g>
      </g>
    </svg>
  }
}
