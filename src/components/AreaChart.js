import React from 'react';
import {BarChart} from './BarChart';

export default function(props) {
  if (!props.data) return null;

  let data = {
    label: "Material",
    values: [
      {name: 'Big', value: props.data.Big ? props.data.Big.length : 0},
      {name: 'Medium', value: props.data.Medium ? props.data.Medium.length : 0},
      {name: 'Small', value: props.data.Small ? props.data.Small.length : 0},
    ],
  }

  return (
    <div className="area chart-holder">
      <BarChart
        data={data.values}
        title="Area"
        sendFilter={props.sendFilter}
        filter={props.filter}
      />
    </div>
  );
}


