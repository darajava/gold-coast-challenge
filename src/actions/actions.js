export const updateMap = (boundingBox) => dispatch => {
  dispatch({
    type: 'UPDATE_MAP',
    boundingBox: boundingBox,
  });
}

export const updateFilter = (filter, chartType) => dispatch => {
  dispatch({
    type: 'UPDATE_FILTER',
    filter: filter,
    chartType: chartType,
  });
}
