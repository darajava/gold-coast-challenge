import ramps from "../data/boat_ramps.json";
import _ from 'lodash';


const generateInitialState = (geojson) => {
  const features = geojson.features.map((feature) => {
    return {
      // points seem to be backwards in geojson
      geometry: feature.geometry.coordinates[0][0].map((e) => e.reverse()),
      type: feature.properties.type,
      material: feature.properties.material,
      area: feature.properties.area_,
      shownMaterial: true,
      shownArea: true,
      shownViewport: true,
    };
  });

  return {
    ramps: features,
  }
}

const defaultState = generateInitialState(ramps);

console.log('d', defaultState);

const filterGeo = (p, bb) => {
  console.log(bb);
  if (!bb) return true;
  return bb && bb.contains(p);
}

const areaGroup = (ramp) => {
  if (ramp.area < 50) {
    return "Small";
  } else if (ramp.area < 200) {
    return "Medium";
  } else {
    return "Big";
  }
}

const isShown = (ramp) => {
  return ramp.shownMaterial && ramp.shownArea && ramp.shownViewport;
}

export default (state = defaultState, action) => {
  console.log('fff', action.type, action)
  
  switch (action.type) { 

    case 'UPDATE_MAP':

      let geoFilteredRamps = state.ramps.map((ramp) => {
        ramp.shownViewport = filterGeo(ramp.geometry[0], action.boundingBox);

        return {...ramp}
      });
      console.log(_.groupBy(geoFilteredRamps, areaGroup));

      return {
        ...state,
        ramps: geoFilteredRamps,
        areaChart: _.groupBy(geoFilteredRamps.filter(isShown), areaGroup),
        materialChart: _.groupBy(geoFilteredRamps.filter(isShown), (ramp) => ramp.material),
      };

    case 'UPDATE_FILTER':
      const filteredRamps = state.ramps.map((ramp) => {
        if (action.filter === state["filter" + action.chartType]) {
          action.filter = undefined;
        }

        if (action.chartType === "Material") {
          ramp.shownMaterial = !action.filter || ramp.material === action.filter;
        } else if (action.chartType === "Area") {
          ramp.shownArea = !action.filter || areaGroup(ramp) === action.filter;
        }
        
        return {...ramp};
      });

      return {
        ...state,
        ramps: filteredRamps,
        ["filter" + action.chartType]: action.filter,
        areaChart: _.groupBy(filteredRamps.filter(isShown), areaGroup),
        materialChart: _.groupBy(filteredRamps.filter(isShown), (ramp) => ramp.material),
      }
    default:
      return state
  }
}