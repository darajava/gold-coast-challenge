import React, { Component } from 'react';
import './App.css';
import { connect } from 'react-redux';
import { updateMap, updateFilter } from './actions/actions';
import L from 'leaflet';
import AreaChart from './components/AreaChart';
import MaterialChart from './components/MaterialChart';

const mapStateToProps = state => ({
  ramps: state.ramps,
  areaChart: state.areaChart,
  materialChart: state.materialChart,
  filterArea: state.filterArea,
  filterMaterial: state.filterMaterial,
})

const mapDispatchToProps = dispatch => ({
 updateMap: (boundingBox) => dispatch(updateMap(boundingBox)),
 updateFilter: (filter, chartType) => dispatch(updateFilter(filter, chartType))
})

class App extends Component {
  constructor() {
    super();

    this.markers = [];
    this.polygons = [];
  }

  componentDidMount() {
    this.map = L.map('map', {
      center: [-27.9412933770449, 153.41013298486263],
      zoom: 10,
      layers: [
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png'),
      ]
    });

    this.map.on("moveend", () => {
      console.log(this.map.getBounds());
      this.props.updateMap(this.map.getBounds());
    })

    this.props.updateMap(this.map.getBounds());
  }

  componentDidUpdate() {
    // clear the map
    for (let i = 0; i < this.markers.length; i++) {
      this.map.removeLayer(this.markers[i]);
    }
    for (let i = 0; i < this.polygons.length; i++) {
      this.map.removeLayer(this.polygons[i]);
    }

    for (let i = 0; i < this.props.ramps.length; i++) {
      let latlngs = this.props.ramps[i].geometry;
      
      if (this.props.ramps[i].shownMaterial && this.props.ramps[i].shownArea) {
        this.polygons.push(L.polygon(latlngs, {color: 'red'}).addTo(this.map));        
        this.markers.push(L.marker(latlngs[0]).addTo(this.map));
      }
    }
  }

  sendFilter = (value, chartType) => {
    this.props.updateFilter(value, chartType)
  }

  render() {
    return (
        <div className="App">
          <div id='map' />
          <AreaChart
            filter={this.props.filterArea}
            sendFilter={(value) => this.sendFilter(value, "Area")}
            data={this.props.areaChart}
          />
          <MaterialChart
            filter={this.props.filterMaterial}
            sendFilter={(value) => this.sendFilter(value, "Material")}
            data={this.props.materialChart}
          />
        </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
