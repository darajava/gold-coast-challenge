# Gold Coast Ramps

An interactive visualisation tool to display information and statistics about boat ramps in the Gold Coast region in Australia.

To run simply: 

    npm i
    npm run start

## Architecture

This was built with React for the UI and Redux for state management. Leaflet.js was utilised for map visualisation and map functions. You can view a demo here: https://darajava.gitlab.io/gold-coast-challenge/
